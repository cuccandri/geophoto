# GeoPhoto

### Project source can be downloaded from our [repository].

### Author and contributor list

> Cuccagna Marco

> Evandri Eros

### Version
1.0.0

### Job Goal
GeoPhoto is a mobile app for the italian Civil Protection.The project consists in an application that allows the user to capture or upload pictures as photographic surveys for river safety. GeoPhoto enables to add information about the site to pictures. 

### Functional / not-functional requirements
* Req_SMART_SURVEY_010: The application has to show the splashscreeen.
* Req_SMART_SURVEY_020: the user has to authenticate to use the application
* Req_SMART_SURVEY_030: The application has to capture images with geo tag
* Req_SMART_SURVEY_040: The application ha to manage the possibility of reject an image and repete the process of image acquisition untill the success/delete from the user.
* Req_SMART_SURVEY_050:  The application has to save the images geo-tagged on the devices with its metadata.
* Req_SMART_SURVEY_060: The application has to ask the user to add some data to the image.
* Req_SMART_SURVEY_070: The application has to handle the asynchronous upload on the "cloud" if there isn't data connection.
* Req_SMART_SURVEY_080: The application has to show the summary of the survey the user did with a map service, it has to show which are uploaded and which are to be synchronized.
* Req_SMART_SURVEY_090: The application has to show the summary of the survey the user did with the possibility to cancel the survey.
* Req_SMART_SURVEY_100: The application has to handle the asynchronous upload considering the priority the user set.

### Toolchain
* [Phonegap] - PhoneGap is an application container technology that allows you to create natively-installed applications for mobile devices using HTML, CSS, and JavaScript. The core engine for PhoneGap is also 100% open source, under the Apache Cordova project. Adobe PhoneGap framework provide the advantage of technology created by a diverse team of pros along with a robust developer community, so you can get to mobile faster.
* [PhpStorm] - PhpStorm is a IDE for a mobile developer
* [SDK Android] - Android Studio provides the fastest tools for building apps on every type of Android device.
* [node.js] - Node.js is a JavaScript runtime built on Chrome's V8 JavaScript engine. Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient.  
* [leaflet] - Leaflet is the leading open-source JavaScript library for mobile-friendly interactive maps. 

### System architecture(API)
The reason for choosing PhoneGap is that it's a native WebView component with HTML5-CSS3 application and it has a structured API that uses JavaScript to access native functionalities of mobile devices. This means that an app you build on PhoneGap can access native functions from the devices as well as the mobile operating system and the PhoneGap API handles communication with the native operating system. You can build high-performance apps on PhoneGap and make them work on several mobile platforms. A robust library of plugins extend the capabilities of the hybrid mobile application. In addition to the “out of the box” functionality, you can also leverage PhoneGap’s JavaScript-to-native communication mechanism to write “native plugins”. PhoneGap native plugins enable you to write your own custom native classes and corresponding JavaScript interfaces for use within your PhoneGap applications. 

![image](./screenshot/API.png)

Plugins used:
> `cordova-plugin-camera`: This plugin defines a global `navigator.camera` object, which provides an API for taking pictures and for choosing images from the system's image library.
Although the object is attached to the global scoped navigator, it is not available until after the `deviceready` event.

> `cordova-plugin-file`: This plugin implements a File API allowing read/write access to files residing on the device. This plugin is based on several specs, including : The HTML5 File API.This plugin defines global `cordova.file` object.
Although in the global scope, it is not available until after the `deviceready` event.

> `cordova-plugin-file-transfer`: This plugin allows you to upload and download files.
This plugin defines global `FileTransfer`, `FileUploadOptions` constructors. Although in the global scope, they are not available until after the `deviceready` event.

> `cordova-plugin-geolocation`: This plugin provides information about the device's location, such as latitude and longitude. Common sources of location information include Global Positioning System (GPS) and location inferred from network signals such as IP address, RFID, WiFi and Bluetooth MAC addresses, and GSM/CDMA cell IDs. This plugin defines a global `navigator.geolocation` object (for platforms where it is otherwise missing). Although the object is in the global scope, features provided by this plugin are not available until after the `deviceready` event.

> `cordova-plugin-network-information`: This plugin provides an implementation of an old version of the Network Information API. It provides information about the device's cellular and wifi connection, and whether the device has an internet connection.

### Server Architecture [^1]
The application server handles business logic and communicates with a back-end data repository(DB). 
The application server is a web server and has a server side scripting language such as PHP. PhoneGap is agnostic of back-end technologies and can work with any application server using standard web protocols. The application server performs business logic and calculations, and generally retrieves or persists data from a separate data repository. The relational database is called `my_cuccandri` and it's composed of two table called `segnalazioni` and `user`.

![image](./screenshot/architecture.png)

The table `segnalazioni` is used to upload the data referred to the image uploaded.

![image](./screenshot/segnalazioni.PNG)

Obviously the table `user` is necessary to log the user.

![image](./screenshot/user.PNG)

Online there are also some file `php` used for the `ajax` call necessary for the asynchronous call to the server such as the log, the location for the map, the upload and manage of the image and the data associated. 
	
### Client Architecture [^1]
The user interface for PhoneGap applications is created using HTML, CSS, and JavaScript. The UI layer of a PhoneGap application is a web browser view that takes up 100% of the device width and 100% of the device height.
The web view used by PhoneGap is the same web view used by the native operating system. On Android, this is android.webkit.WebView. 
The PhoneGap application acts as a client for the user to interact with. The PhoneGap client communicates with an application server [Cuccandri] to receive data.
PhoneGap applications generally do not talk directly to a database, communication is routed through an application server. The client to application server communication can be based upon standard HTTP requests for HTML content, REST-ful `XML` services, `JSON` services, or `SOAP` (or websockets if your OS supports it). These are the exact same techniques that you would use in a desktop-browser based `AJAX` application.
The client-side architecture used is the single-page application model, where the application logic is inside a single HTML page. This page is never unloaded from memory. All data will be displayed by updating the HTML DOM, data is retrieved from the application server using AJAX techniques, and variables are kept in-memory within JavaScript (`localStorage`).

![image](./screenshot/export.png)

### Workflow and Screenshot

The first thing the user has to do is to open the application by clicking the icon called `GeoPhoto`.

![image](./screenshot/android.png)


The first window the user sees is the splashscreen with the timeout set to 5 seconds.
 
![image](./screenshot/splash.png)

After that the user will be redirected to the page home, where the only action he can do is the login. 

![image](./screenshot/login.png)

Once logged the user is redirected to the effective home page of the app.

![image](./screenshot/index.png)

From the main page the user can see the gallery section and the map, moreover, he can record a survey. There is a sidebar too, where the user can see the link to the main pages of the app, included the logout page.
If the user clicks on the FAB button he can choose to take a picture or upload it from the gallery of his device.

![image](./screenshot/pulsanti.png)

If the user clicks the capture button the default camera application will be opened.

![image](./screenshot/camera.png)

The user can take a picture and, if he doesn't like it, he can retry the capture. When the user is satisfied he clicks the button to accept. The picture will be saved in the default folder of the project `GeoFolder` with the name of the date of the capture. If everything goes right the user will be redirected to a new page where he can see the picture and the form for the metadata he wants to add.

![image](./screenshot/form.png)

When the user clicks the button `Carica` will be called a function that saves the survey in cache, upload the image in the forder `upload` on the server and upload the data of the form to the `segnalazioni` table.
If everything goes right the user will be redirected to the map where he can see the survey he just registered.
If the user decides to upload the picture from the gallery, the default file manager will be opened, he chooses the image to upload and after that the workflow is the same as the capture image workflow.

![image](./screenshot/filemanager.png)

From the index page the user can click on the gallery button, he will be redirect to the gallery.
If the user never uploaded a survey then he will see a message informing him that the gallery is empty.

![image](./screenshot/vuota.png)

He can see the image saved on the cache storage with its metadata, he can also see if the image is uploaded or not, in fact if it's uploaded there will be a blue icon, if it's not uploaded the user can see a red icon and the upload button.

![image](./screenshot/galleriaok.png)

![image](./screenshot/galleriaproblem.png)

If the user sees that an image is not uploaded and decides to send it to the server, he clicks the dedicated button and the function `riprova` launches the function `reuploadimage` and `reuploaddata` to upload the survey. At the end of the process he can see the proper message.

![image](./screenshot/galleriaalert.png)

Each image can be deleted or modified by the user. If the user clicks the button `cancella`, the survey will be cancelled and a confirm alert will tell if the operation is successful.

![image](./screenshot/galleriaconfirm.png)

If the user clicks the button `modifica` he will be redirected to the page form. The form is filled with the data referred to the image, he changes the data and uploads them.

![image](./screenshot/form.png)

From the main page of the app the user can click the button `mappa` and he will be redirect to the page map. Until the map is ready the user can only see a load icon.

![image](./screenshot/mappaload.png)

The user can finally see the map, there will be a circle that shows the actual position, and its dimension is proportional to the accuracy of the location.

![image](./screenshot/mappacerchio.png)

Here the user can see the map with all the survey uploaded, a blue marker for each survey with `Urgenza`=1, if the survey is with `Urgenza`=1 the marker will be red. The map is realized with the leaflet library.

![image](./screenshot/mappapuntatori.png)

If the user click on the marker, he will see the image with all the data uploaded.

![image](./screenshot/mappapopup.png)


At the end there is the logout page where the user can logout.

![image](./screenshot/logout.png)


### Problems 
With Phonegap we are not building a native mobile application. The main problem we incurred is the impossibility of handling the background service. 
It's not possible to manage the sensors of the devices, for example we cannot switch from data to wifi connection or switch on the GPS. Another problem we incurred is the different response from the apk generated and the emulator: with the emulator is quicker to run the app but not all the plugins work; for example it's not possible to do operations that involve the filesystem.
When we finished the application we decided to do some tests, we gave the apk file to some user to see if the interface and the functions are easy to understand. We set our application following Google guide lines for material design, in fact we have a sidebar and the FAB button for the actions. 

![image](./screenshot/exindex.png)

After this tests we understood the user can not find the capture camera or upload picture buttons easily so we choose to add another button near the `galleria` and `mappa` buttons in the index to redirect to the camera section.

![image](./screenshot/index.png)

### Future developments
A possible future development is the integration with augmented reality, another development could be to add a navigator system to reach a survey from another site and the possibility of search a survey from search field.

[Phonegap]: <http://phonegap.com>
[node.js]: <http://nodejs.org>
[Leaflet]: <http://leafletjs.com>
[jQuery]: <http://jquery.com>
[PhpStorm]: <https://www.jetbrains.com/phpstorm/>
[SDK Android]: <https://developer.android.com/studio/index.html>
[Cuccandri]: <http://www.cuccandri.altervista.org/>
[repository]: <https://bitbucket.org/cuccandri/geophoto>

[^1]: <http://phonegap.com/blog/2012/05/02/phonegap-explained-visually>