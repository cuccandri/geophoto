var def_lat=43.58;
var def_long=13.51;
var timer=0;
//var longit = localStorage.Longitude;
//var latit = localStorage.Latitude;
cartella = "Geofolder";
var percorso ="../../storage/emulated/0/"+cartella+"/";

if(localStorage.Timer >=0 ){
    timer=localStorage.Timer;
}


var oldItems= JSON.parse(localStorage.getItem('segnalazioni')) || [];
//var oldItems = [{"id_segnalazione":"99","user":"logtest","latit":"43.5951508","longit":"13.5049933","criticita":"2","urgenza":"1","note":"notanotanota","foto":"2016-07-19-10-50-21.jpg"},{"id_segnalazione":"100","user":"logtest","latit":"43.5951072","longit":"13.5049921","criticita":"1","urgenza":"0","note":"","foto":"2016-07-19-18-58-00.jpg"},{"id_segnalazione":"101","user":"logtest","latit":"43.5950965","longit":"13.5049975","criticita":"1","urgenza":"1","note":"","foto":"2016-07-19-19-01-09.jpg"}];
//alert("elementi: "+oldItems.length);

var indice = localStorage.indice;
//var indice=2;

document.addEventListener("deviceready",onDeviceReady,false);
//riempi();
function onDeviceReady() {
    riempi(); //visualizza l'immagine della rilevazione da modificare
    
}
function riempi() {
    //alert("latit " + oldItems[indice].latit);
    //metto la foto dentro l'attributo src del tag <img>

    var path = percorso + oldItems[indice].foto;
    var Image = document.getElementById('Image');
    Image.src = path;
    // alert("immetti foto :"+ path);

    document.forms['up'].foto.value=oldItems[indice].foto;
    document.forms['up'].latit.value=oldItems[indice].latit;
    document.forms['up'].longit.value=oldItems[indice].longit;

    if(oldItems[indice].criticita == '1')       document.forms['up'].bassa.checked = true;
    else if(oldItems[indice].criticita == '2')  document.forms['up'].media.checked = true;
    else if(oldItems[indice].criticita==3)      document.forms['up'].alta.checked = true;



    document.forms['up'].Note.value=oldItems[indice].note;
    if(oldItems[indice].urgenza==1)document.forms['up'].Urgenza.checked=true;
    
}


function uploadimage() {
    var Image = document.getElementById('Image').getAttribute("src");
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName=foto; //nome della foto che verra salvato online
    options.mimeType="image/jpeg";

    var params = new Object();
    params.nome = "test"; //??

    options.params = params;
    options.chunkedMode = false;

    var ft = new FileTransfer();
    ft.upload(Image, "http://cuccandri.altervista.org/uploadphoto.php", win, fail, options);
    return false;
}

function win(r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
    alert("upload foto completato!");
}

function fail(error) {
    //alert("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
    alert("ops! il server non risponde!");

}


function fallito(error) {
    alert("ha fallito: "+error);
}


$("#upload").click(function(){
    var user= localStorage.username;
    var criticita = 0;
    if(document.getElementById('bassa').checked) {
        criticita = 1;
    }else if(document.getElementById('media').checked) {
        criticita = 2;
        //alert("cr 2");
    }else if(document.getElementById('alta').checked) {
        criticita = 3;
        //alert("cr 3");
    }

    var Urge= document.getElementById('Urgenza');
    var latit= $("#latit").val();
    var longit= $("#longit").val();

    //alert("chiamata: " + latit);
    var note = $("#Note").val();

    var urgenza='0';
    if (Urge.checked){
        urgenza='1';
    }

    salvaimage(user, criticita, urgenza,latit,longit,note,oldItems[indice].foto); //salva i dati in local storage
   // uploadimage();
   // alert("user "+user + " latit e longit "+latit+longit+" crit "+criticita+" urg "+urgenza+" note " + note+ " foto "+foto);
    $.ajax({
        type: "POST",
        url: "http://cuccandri.altervista.org/update.php",
        data: {user: user, longit: longit, latit: latit, criticita: criticita, urgenza: urgenza, note: note, foto: oldItems[indice].foto},
        success: function(responce){
            if(responce=='success')
            {
                alert('update completato, controlla la mappa!');
                window.location.replace("mappa.html");

            }
            else
            {
                alert("errore, il server non risponde, riprova più tardi.");
                window.location.replace("galleria.html");
            }
        }
    });
    return false;
});


function salvaimage(user, criticita, urgenza,latit,longit,note,foto) {

    //var oldItems = JSON.parse(localStorage.getItem('segnalazioni')) || [];

    var newItem = { "user":user, "latit": latit, "longit":longit, "criticita": criticita, "urgenza": urgenza, "note" : note, "foto" : foto };
    //var newItem = { "user":user, "latit": 11, "longit":22, "criticita": criticita, "urgenza": urgenza, "note" : note, "foto" : 123 };
    oldItems.splice(indice, 1);
    oldItems.push(newItem);

    localStorage.setItem('segnalazioni', JSON.stringify(oldItems));
   // alert("natit: "+ newItem.latit );
    //alert("dati salvati in locale");

 }
