function onfail(error, caller) {
    error = error || '[error]';
    caller = caller || '[caller]';
    alert('Error > ' + caller + " code: " + error.code);
};

/*
 Error codes
 NOT_FOUND_ERR = 1;
 SECURITY_ERR = 2;
 ABORT_ERR = 3;
 NOT_READABLE_ERR = 4;
 ENCODING_ERR = 5;
 NO_MODIFICATION_ALLOWED_ERR = 6;
 INVALID_STATE_ERR = 7;
 SYNTAX_ERR = 8;
 INVALID_MODIFICATION_ERR = 9;
 QUOTA_EXCEEDED_ERR = 10;
 TYPE_MISMATCH_ERR = 11;
 PATH_EXISTS_ERR = 12;
 */


function doCameraAPI() {
    // Retrieve image file location from specified source
    navigator.camera.getPicture(getImageURI, function (message) {
        alert('Image Capture Failed');
    }, {
        quality: 40,
        destinationType: Camera.DestinationType.FILE_URI
    });

}; //doCameraAPI

function getImageURI(imageURI) {

    //resolve file system for image to move.
    window.resolveLocalFileSystemURI(imageURI, gotFileEntry, function (error) {
        onfail(error, 'Get Target Image')
    });


    function gotFileEntry(targetImg) {
        //alert("got image file entry: " + targetImg.name);     
        //now lets resolve the location of the destination folder
        window.resolveLocalFileSystemURI(POSTPATH, gotDestinationEntry, function (error) {
            onfail(error, 'Get Destination Dir')
        });
        function gotDestinationEntry(destination) {
            // move the file 
            targetImg.moveTo(destination, targetImg.name, moveSuccess, function (error) {
                onfail(error, 'Move Image')
            });
            alert('dest :' + destination.fullPath);
        };

        function moveSuccess() {
            alert('FILE MOVE SUCCESSFUL!');
        };
    };