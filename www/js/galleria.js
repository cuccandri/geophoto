localStorage.cartella = "geofolder";
cartella = localStorage.cartella;
var percorso ="../../storage/emulated/0/"+cartella+"/";


var oldItems= JSON.parse(localStorage.getItem('segnalazioni')) || [];

//var oldItems= [{"id_segnalazione":"99","user":"logtest","latit":"43.5951508","longit":"13.5049933","criticita":"1","urgenza":"0","note":"","foto":"2016-07-19-10-50-21.jpg"},{"id_segnalazione":"100","user":"logtest","latit":"43.5951072","longit":"13.5049921","criticita":"1","urgenza":"0","note":"","foto":"2016-07-19-18-58-00.jpg"},{"id_segnalazione":"101","user":"logtest","latit":"43.5950965","longit":"13.5049975","criticita":"1","urgenza":"1","note":"","foto":"2016-07-19-19-01-09.jpg"}];

var altervista =[];
var conta = 0;


document.addEventListener("deviceready",onDeviceReady,false);
//recupera();

function onDeviceReady() {
    recupera();
    //alert("sei : "+ localStorage.username );
}

function recupera( ) {
    $(document).ready(function () {
        $.ajax({    //create an ajax request to load_page.php
            type: "POST",
            url: "http://cuccandri.altervista.org/dettaglio.php ",
            data: {user: localStorage.username},

            dataType: "html",   //expect html to be returned
            beforeSend: function () {
                //$("#gay").html('recupero i dati...');
            },
            success: function (response) {
                if (response=='invalid username') {
                    alert("invalid credentials");
                }else{

                    altervista = JSON.parse(response);
                    //alert("ajax: success "+altervista.length);
                    /* stampa correttamente tutti i nomi
                     altervista.forEach(function (pippa) {
                     alert (pippa.foto);
                     });
                     */
                    galleria();
                }
            },
            error: function () {
                galleria();
            }
        });
    });
}



function galleria() {
    //alert("old items: "+ oldItems.length);
    oldItems.forEach(stampa);
    if(conta == 0 )$("#static").append("<br><br><h5 style='text-align: center'> non hai ancora scattato nessuna foto... cosa aspetti?</h5>");
}

function stampa(item, index) {
   // alert(item.user + " " +  localStorage.username);
    if(item.user == localStorage.username){
        conta++;
        synk=0;
        for(a=0; a<altervista.length; a++){
        //for(a=altervista.length-1; a>=0; a--){
            // alert(altervista[a].foto +" =? "+ item.foto);
            if(altervista[a].foto == item.foto) {
                // alert("numero giri: "+a);
                synk=1;
                break;
            }
        }

        var str = "<div class='row'>";
        str += "<div class='col s12 m7'>";
        str += "<div class='card'>";
        str += "<div class='card-image'>";
        str += "<img src='" + percorso +item.foto + "'>";
        str += "<span class='card-title'>"+item.foto+"</span>";
        str += "<a style='margin: -27.5px 5px 0;float: right;' ";
        if(synk)str += "class='btn-floating light-blue btn-large'><i class='material-icons'>done</i></a>";
        else str += "class='btn-floating red btn-large'><i class='material-icons'>report_problem</i></a>";
        str += "</div><div class='card-content'>";

        if(item.criticita==1)      cri = "bassa ";
        else if(item.criticita==2) cri ="media ";
        else if(item.criticita==3) cri ="alta ";
        var urg= " ";
        if(item.urgenza == 1) urg="<b> URGENTE </b>";
        var nota = '';
        if(item.note != '') nota=" Note: "+ item.note;

        str += "<p>Criticità: "+ cri + urg + nota + " </p>";
        str += "</div><div class='card-action'style='text-align: left'>";
        str += "<a style='margin: 0 6px;' class='btn-floating black'><i class='material-icons'onclick=' elimina("+index+" )' >delete</i></a>";
        str += "<a style='margin: 0 6px;' class='btn-floating yellow darken-2'><i class='material-icons' onclick='modifica("+index+")'>mode_edit</i></a>";
        if(!synk) str += "<a style='margin: 0 6px;' class='btn-floating light-blue'><i class='material-icons' onclick=' riprova("+index+" )'>swap_vert</i></a>";
        str += "</div></div></div></div>";

        $("#static").append(str);

    }
}


function  riprova ( indice ) {
    REuploadimage(indice);
    REuploadData(indice);

}
function REuploadimage(indice) {
    var Image = percorso+oldItems[indice].foto;
    var nomefoto= oldItems[indice].foto;
    
    var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName = nomefoto ; //nome della foto che verra salvato online
    options.mimeType="image/jpeg";

    var params = new Object();
    params.nome = "test"; //??

    options.params = params;
    options.chunkedMode = false;

    var ft = new FileTransfer();
    ft.upload(Image, "http://cuccandri.altervista.org/uploadphoto.php",
        function () { //win
            alert("upload foto riuscito");
        },
        function () {
            alert("impossibile contattare il server, riprova più tardi") ;
        },
        options);
}
function REuploadData(indice) {
    var el= oldItems[indice];

    //alert("user: "+ el.user + "latit: "+el.latit );

    $.ajax({
        type: "POST",
        url: "http://cuccandri.altervista.org/upload.php",
        data: {user: el.user, longit: el.longit, latit: el.latit, criticita: el.criticita, urgenza: el.urgenza, note: el.note, foto: el.foto},
        success: function(responce){
            if(responce=='success')
            {
                alert('upload completato, controlla la mappa!');
                window.location.replace("mappa.html");

            }
            else
            {
                alert("errore, il server non risponde, riprova più tardi.");
                window.location.replace("galleria.html");
            }
        }
    });
}



function elimina(indice) {
    if(confirm("sei sicuro di voler eliminare questa segnalazione?")== true ){
        var el = oldItems[indice];

        //alert("cancelliamolo allora!user: " + el.user + " foto: " + el.foto);
        $.ajax({
            type: "POST",
            url: "http://cuccandri.altervista.org/delete.php",
            data: {user: el.user, foto: el.foto},
            success: function(responce){
                if(responce=='success')
                {
                    alert('cancellazione confermata');
                    oldItems.splice(indice, 1);
                    localStorage.setItem('segnalazioni', JSON.stringify(oldItems));
                    window.location.replace("galleria.html");
    
                }
                else
                {
                    alert("errore, il server non risponde, riprova più tardi.");
                    window.location.replace("galleria.html");
                }
            }
        });
    }
}

function modifica(indice) {
    localStorage.indice = indice;
    window.location.href = "modifica.html";
}